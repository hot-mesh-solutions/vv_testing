/* Sender Firmware
 * */

#include <Arduino.h>
#include <HardwareSerial.h>
#include <SoftwareSerial.h>
#include <RH_RF95.h>

#define DEBUG

#include "helpers.h"

/********************************************************************************************/
#define BAUDRATE (115200)
#define TX_TIMEOUT (5000)
#define LOOP_DELAY (1000)
#define WATCHDOG_TIME_ADD_MS (8192 + 2) // 8 second WDT is actually 8.192 + 2ms wakeup @ 8MHz

#define SENSOR_PWR (A2)
#define SENSOR_RXD (A1)
#define SENSOR_TXD (A0)
#define SENSOR_WAIT_ATTEMPTS (30)

#define RFM95_FREQ (915.0) // MHz
#define RFM95_TX_PWR (6) // dBm
#define RFM95_SS_PIN (9)
#define RFM95_INT_PIN (3) // RFM95 Interrupt pin
#define RFM95_RST_PIN (2) // RFM95 Reset pin
/********************************************************************************************/

char buf[RH_RF95_MAX_MESSAGE_LEN];
char data[RH_RF95_MAX_MESSAGE_LEN];
int data_length;
unsigned long last_millis = 0;
unsigned long seconds_since_startup = 0;
extern volatile bool _WDT_TRIGGERED;

RH_RF95 rf95_radio(RFM95_SS_PIN, RFM95_INT_PIN);
SoftwareSerial sensorSerial(SENSOR_RXD, SENSOR_TXD); // RX, TX


void setup() {
    Serial.begin(BAUDRATE);
    while (!Serial)
        ; // Wait for serial port to be available

    DEBUG_PRINTLN("Hot Mesh Solutions V&V Data Sender");
    delay(500);

    while (!rfm95_easy_init(&rf95_radio, RFM95_RST_PIN, RH_RF95::Bw125Cr45Sf128, RFM95_FREQ, RFM95_TX_PWR)) {
        DEBUG_PRINTLN("RFM95 init failed");
        delay(2000);
    }

    change_wdt_sleep_time(WDT_8S);

    DEBUG_PRINT("Setting up sensor");
    pinMode(SENSOR_PWR, OUTPUT);
    digitalWrite(SENSOR_PWR, HIGH);

    sensorSerial.begin(300);
    while (!sensorSerial) {
        DEBUG_PRINT(".");
        delay(100);
    }
    DEBUG_PRINTLN(" Established");
    digitalWrite(SENSOR_PWR, HIGH);
    DEBUG_PRINTLN("Setup complete");
}

void loop() {
    memset(buf, 0, sizeof(buf));
    memset(data, 0, sizeof(data));
    char sensor_reading;

    while(sensorSerial.available()) {
        sensorSerial.read();
        delay(10);
    }
    DEBUG_PRINTLN("Buffer flushed");

    DEBUG_PRINT("Starting sensor");
    digitalWrite(SENSOR_PWR, HIGH);

    int i;
    for (i = 0;!sensorSerial.available() && i < SENSOR_WAIT_ATTEMPTS;i++) {
        DEBUG_PRINT(".");
        delay(100);
    }

    if (i != SENSOR_WAIT_ATTEMPTS) {
        for (int i = 0;(sensor_reading = sensorSerial.read()) != '\n';i++)
            buf[i] = sensor_reading;
    } else {
        DEBUG_PRINTLN("Read sensor failed!");
    }

    digitalWrite(SENSOR_PWR, LOW);
    DEBUG_PRINT("Done: ");
    DEBUG_PRINTLN(buf);

    seconds_since_startup += (millis() - last_millis) / 1000;
    last_millis = millis();

    // seconds since startup,temperature
    sprintf(data, "%lu,%s", seconds_since_startup, buf);

    DEBUG_PRINT("Sending data: ");
    DEBUG_PRINTLN(data);
    data_length = 0;
    while (data[data_length] != 0)
        data_length++;
    DEBUG_PRINT("Data length is ");
    DEBUG_PRINTLN(data_length);
    rf95_radio.send(data, data_length);

    if (!rf95_radio.waitPacketSent(TX_TIMEOUT)) {
        DEBUG_PRINTLN("Timeout");
        return;
    }

    DEBUG_PRINTLN("Powerdown");
    delay(100);

    go_to_sleep();
    seconds_since_startup += WATCHDOG_TIME_ADD_MS / 1000;
}
