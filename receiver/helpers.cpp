#include "helpers.h"

volatile bool _WDT_TRIGGERED;

ISR(WDT_vect) { // Handles the Watchdog Time-out Interrupt
    _WDT_TRIGGERED = true;
}

void go_to_sleep(void) {
    byte adcsra = ADCSRA;          //save the ADC Control and Status Register A
    ADCSRA = 0;                    //disable the ADC
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        _WDT_TRIGGERED = false;
        sleep_enable();
        sleep_bod_disable();       //disable brown-out detection (saves 20-25µA)
    }
    sleep_cpu();                   //go to sleep
    sleep_disable();               //wake up here
    ADCSRA = adcsra;               //restore ADCSRA
}

void change_wdt_sleep_time(int wdt_time) {
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        wdt_reset();
        MCUSR &= ~_BV(WDRF);                            //clear WDRF
        WDTCSR |= _BV(WDCE) | _BV(WDE);                 //enable WDTCSR change
        WDTCSR =  _BV(WDIE) | wdt_time;
    }
}

bool rfm95_easy_init(RH_RF95 *radio, uint8_t reset_pin, RH_RF95::ModemConfigChoice config_choice, float freq_center, int8_t tx_pwr) {
    pinMode(reset_pin, OUTPUT);
    digitalWrite(reset_pin, HIGH);
    delay(10);
    digitalWrite(reset_pin, LOW);
    delay(10);
    digitalWrite(reset_pin, HIGH);
    delay(10);

    if (!radio->init())
        return false;

    radio->setModemConfig(config_choice);
    radio->setFrequency(freq_center);
    radio->setTxPower(tx_pwr, false);

    return true;
}

int rssi_to_dbm(int rssi) {
    return -137 + rssi;
}
