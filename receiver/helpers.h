#ifndef VV_TESTING_HELPERS_H
#define VV_TESTING_HELPERS_H

#include <RH_RF95.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <util/atomic.h>

#ifdef DEBUG
#define DEBUG_PRINT(...) do{ Serial.print( __VA_ARGS__ ); } while( false )
#define DEBUG_PRINTLN(...) do{ Serial.println( __VA_ARGS__ ); } while( false )
#else
#define DEBUG_PRINT(...) do{ } while ( false )
#define DEBUG_PRINTLN(...) do{ } while ( false )
#endif

#define WDT_500MS (            _BV(WDP2) |             _BV(WDP0))
#define WDT_1S    (            _BV(WDP2) | _BV(WDP1)            )
#define WDT_2S    (            _BV(WDP2) | _BV(WDP1) | _BV(WDP0))
#define WDT_4S    (_BV(WDP3)                                    )
#define WDT_8S    (_BV(WDP3) |                         _BV(WDP0))

void change_wdt_sleep_time(int);
bool rfm95_easy_init(RH_RF95 *, uint8_t, RH_RF95::ModemConfigChoice, float, int8_t);
void go_to_sleep(void);
int rssi_to_dbm(int);

#endif //VV_TESTING_HELPERS_H
