/* Receiver Firmware
 * */

#include <Arduino.h>
#include <HardwareSerial.h>
#include <RH_RF95.h>

//#define DEBUG

#include "helpers.h"

/********************************************************************************************/
#define BAUDRATE (115200)
#define RX_TIMEOUT (30000)
#define LOOP_DELAY (2000)

#define RFM95_FREQ (915.0) // MHz
#define RFM95_TX_PWR (6) // dBm
#define RFM95_SS_PIN (9)
#define RFM95_INT_PIN (3) // RFM95 Interrupt pin
#define RFM95_RST_PIN (2) // RFM95 Reset pin
/********************************************************************************************/

char buf[RH_RF95_MAX_MESSAGE_LEN];
extern volatile bool _WDT_TRIGGERED;

RH_RF95 rf95_radio(RFM95_SS_PIN, RFM95_INT_PIN);


void setup() {
    Serial.begin(BAUDRATE);
    while (!Serial) ; // Wait for serial port to be available

    DEBUG_PRINTLN("Hot Mesh Solutions V&V Data Receiver");
    delay(100);

    while (!rfm95_easy_init(&rf95_radio, RFM95_RST_PIN, RH_RF95::Bw125Cr45Sf128, RFM95_FREQ, RFM95_TX_PWR)) {
        DEBUG_PRINTLN("RFM95 init failed");
        delay(2000);
    }

    change_wdt_sleep_time(WDT_8S);

    DEBUG_PRINTLN("Setup complete");
}

void loop() {
    DEBUG_PRINTLN("Powerdown");
    delay(100);
    rf95_radio.setModeCAD();

    go_to_sleep();

    if (_WDT_TRIGGERED) {
        _WDT_TRIGGERED = false; // Clear
        DEBUG_PRINTLN("WDT triggered");
    } else {
        rf95_radio.setModeRx();
        rf95_radio.force_read();

        DEBUG_PRINTLN("Wakeup");
        delay(100);

        uint8_t len = sizeof(buf);
        if (rf95_radio.recv((uint8_t *) buf, &len)) {
            Serial.println(buf);
            DEBUG_PRINT("@");
            DEBUG_PRINTLN(rf95_radio.lastRssi(), DEC);
            DEBUG_PRINT("Length is ");
            DEBUG_PRINTLN(len);
        } else {
            Serial.println("Receive failed");
        }
    }
}
